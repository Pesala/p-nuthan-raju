//
//  DetailViewController.swift
//  iContacts
//
//  Created by Nuthan Raju Pesala on 11/05/20.
//  Copyright © 2020 Nuthan Raju Pesala. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var accessoryView: UIView!
    
   
    let menuView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
   lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = UIColor.white
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    var menuArray = ["View Linked Contacts","Delete","Share","Create shortcut","Set ringtone","Route to voicemail"]
    var contact: Contact?
    let cellHeight: CGFloat = 40
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Detail"
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.setLeftBarButton(UIBarButtonItem(image: UIImage(systemName: "arrow.left")!, style: .done, target: self, action: #selector(handleBack)), animated: true)
        imageView.contentMode = .scaleAspectFit
     
        
        nameLabel.textColor = UIColor.darkGray
        moreButton.tintColor = .darkGray
        
        if let contact = contact {
            nameLabel.text = contact.name
            phNumberLabel.text = contact.phNumber
            emailLabel.text = contact.email
            imageView.image = UIImage(data: contact.profileImage!)
        }
        //view
        menuView.isHidden = true
        accessoryView.addSubview(menuView)
        menuView.rightAnchor.constraint(equalTo: imageView.rightAnchor, constant: -4).isActive = true
        menuView.topAnchor.constraint(equalTo: moreButton.bottomAnchor).isActive = true
        menuView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        menuView.heightAnchor.constraint(equalToConstant: 280).isActive = true
       
        
        moreButton.addTarget(self, action: #selector(handleMenuView), for: .touchUpInside)
        
        //collectionView
        menuView.addSubview(collectionView)
        collectionView.leftAnchor.constraint(equalTo: menuView.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: menuView.rightAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: CGFloat(menuArray.count + 1)*cellHeight).isActive = true
        
        collectionView.showsVerticalScrollIndicator = false
        
    }
    
    @objc func handleBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func handleMenuView() {
        if menuView.isHidden == true {
            menuView.isHidden = false
        }else {
        menuView.isHidden = true
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          if section == 0 {
              return 0
          }
          return 30
      }

}
extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MenuCollectionViewCell
        cell.titleLabel.text = menuArray[indexPath.item]
        return cell
    }
    
    
}

extension DetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: cellHeight)
    }
}

class MenuCollectionViewCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: label.font.familyName, size: 14)
        return label
    }()
    
    override func draw(_ rect: CGRect) {
        addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 4).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
    }
}
