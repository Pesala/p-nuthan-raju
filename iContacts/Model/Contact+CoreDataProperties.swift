//
//  Contact+CoreDataProperties.swift
//  iContacts
//
//  Created by Nuthan Raju Pesala on 13/05/20.
//  Copyright © 2020 Nuthan Raju Pesala. All rights reserved.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var name: String?
    @NSManaged public var phNumber: String?
    @NSManaged public var email: String?
    @NSManaged public var profileImage: Data?

}
