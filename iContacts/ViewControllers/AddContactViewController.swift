//
//  AddContactViewController.swift
//  iContacts
//
//  Created by Nuthan Raju Pesala on 12/05/20.
//  Copyright © 2020 Nuthan Raju Pesala. All rights reserved.
//

import UIKit

protocol DataSender {
    func sendContact(contact: Contact)
    func update()
}

class AddContactViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    let cameraButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "camera.fill")!.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleCameraAction), for: .touchUpInside)
        
        return button
    }()
    
    var delegate: DataSender? = nil
    var person: Contact?
    var isEditable = false
    var contactsData = [String: [Contact]]()
    var image: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.image = UIImage(systemName: "person.fill")?.withRenderingMode(.alwaysTemplate)
        profileImage.isUserInteractionEnabled = true
        profileImage.tintColor = UIColor.white
        profileImage.backgroundColor = UIColor.lightGray
        profileImage.contentMode = .scaleAspectFit
        
        profileImage.addSubview(cameraButton)
        profileImage.bringSubviewToFront(cameraButton)
        cameraButton.rightAnchor.constraint(equalTo: profileImage.rightAnchor, constant: -8).isActive = true
        cameraButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        cameraButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        cameraButton.bottomAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: -8).isActive = true
        
        self.navBarSetup()
        self.dataFromIsEdit()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    func dataFromIsEdit() {
        if let person = person {
            nameTextField.text = person.name
            emailTextField.text = person.email
            phNumberTextField.text = person.phNumber
            profileImage.image = UIImage(data: person.profileImage!)
            nameTextField.becomeFirstResponder()
        }
    }
    
    @objc func handleCameraAction() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tintColor = UIColor.black
        
         let imagePicker = UIImagePickerController()
         imagePicker.delegate = self
        
        let takePhoto = UIAlertAction(title: "Take a Photo", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true)
            }else {
                self.showAlert(title: "Error", message: "For this Device Camera is Not Available")
            }
            
        }
        let photoGallery = UIAlertAction(title: "Choose Photo Gallery", style: .default) { (action) in
            imagePicker.sourceType = .photoLibrary
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(takePhoto)
        alertController.addAction(photoGallery)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
         
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navBarSetup() {
       navigationItem.title = "Add Contact"
       navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)]
        
     navigationItem.setLeftBarButton(UIBarButtonItem(image: UIImage(systemName: "xmark")!, style: .done, target: self, action: #selector(handleCancel)), animated: true)
        
       navigationItem.setRightBarButton(UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(handleSave)), animated: true)
        
    }
    
    @objc func handleSave() {
        if isValidEmail(emailid: emailTextField.text!) == false {
            self.showAlert(title: "Invalid Email", message: "Please Enter Valid EmailID")
            return
          
        }
        if nameTextField.text == "" {
            self.showAlert(title: "Name Field is Empty", message: "Name Field is Required")
            return
        }
        if let phNumber = phNumberTextField.text , phNumber.count != 10 {
            self.showAlert(title: "PhNumber is not valid", message: "Please Enter 10 digit number")
            return
        }
        if phNumberTextField.text == "" {
            self.showAlert(title: "phNumberTextField is Empty", message: "phNumberTextField is Required")
            return
        }
        if emailTextField.text == "" {
            self.showAlert(title: "emailTextField is Empty", message: "emailTextField is Required")
            return
        }
        if image == nil {
            if !isEditable {
                image = profileImage.image?.jpegData(compressionQuality: 0.7)
            }
            imageFromInitials(firstName: nameTextField.text, lastName: nil) { (image) in
                let imageData = image.jpegData(compressionQuality: 0.7)
                self.image = imageData
            }
        }
        for tuple in self.contactsData.enumerated() {
            let key = tuple.element.key
            let persons = contactsData[key]
            for person in persons! {
                if person.phNumber == phNumberTextField.text {
                    self.showAlert(title: "phNumber already exists", message: "Please Enter New number")
                    return
                }
                
            }
        }
        if !isEditable {
            let contact = Contact(context: PersistenceServices.context)
            contact.name = nameTextField.text?.uppercased()
            contact.email = emailTextField.text
            contact.phNumber = phNumberTextField.text
            
            contact.profileImage = self.image
            do {
                try PersistenceServices.context.save()
            }catch {
                print("Failed to save")
            }
        delegate?.sendContact(contact: contact)
        self.navigationController?.popViewController(animated: true)
        }else {
         
            person?.name = nameTextField.text!.uppercased()
            person?.phNumber = phNumberTextField.text!
            person?.email = emailTextField.text!
            person?.profileImage = image
            do {
                try PersistenceServices.context.save()
            }catch {
                print("Failed to update")
            }
            delegate?.update()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func handleCancel() {
        self.navigationController?.popViewController(animated: true)
      }

}

extension AddContactViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = selectImage.jpegData(compressionQuality: 0.7)
            if let imgData = image {
            self.profileImage.image = UIImage(data: imgData)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
