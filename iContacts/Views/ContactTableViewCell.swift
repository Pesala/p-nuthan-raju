//
//  ContactTableViewCell.swift
//  iContacts
//
//  Created by Nuthan Raju Pesala on 12/05/20.
//  Copyright © 2020 Nuthan Raju Pesala. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phNumberLabel: UILabel!
    
    var contact: Contact? {
        didSet {
            if let contact = contact {
            nameLabel.text = contact.name
            phNumberLabel.text = contact.phNumber
            profileImageView.image = UIImage(data: contact.profileImage!)
          
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
 
    override func draw(_ rect: CGRect) {
     
        profileImageView.layer.cornerRadius = 30
        profileImageView.layer.masksToBounds = true
        profileImageView.clipsToBounds = true
         
    }
    

}
