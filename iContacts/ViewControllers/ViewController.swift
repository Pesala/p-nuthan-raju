//
//  ViewController.swift
//  iContacts
//
//  Created by Nuthan Raju Pesala on 11/05/20.
//  Copyright © 2020 Nuthan Raju Pesala. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let floatingbutton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "Add")!.withRenderingMode(.alwaysOriginal), for: .normal)
        button.contentMode = .scaleAspectFill
        return button
    }()
    
    var contacts = [Contact]()
    var allContacts: [String: [Contact]] = ["A":[],"B":[],"C":[],"D":[],"E":[],"F":[],"G":[],"H":[],"I":[],"J":[],"K":[],"L":[],"M":[],"N":[],"O":[],"P":[],"Q":[],"R":[],"S":[],"T":[],"U":[],"V":[],"W":[],"X":[],"Y":[],"Z":[]]
    
    var contactSectionTitles: [String] = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "contacts"
        navigationController?.navigationBar.prefersLargeTitles = true
        
      
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
        view.addSubview(floatingbutton)
        
        floatingbutton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8).isActive = true
        floatingbutton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8).isActive = true
        floatingbutton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        floatingbutton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        floatingbutton.layer.cornerRadius = floatingbutton.frame.width / 2
        floatingbutton.addTarget(self, action: #selector(handleAddEvent), for: .touchUpInside)
        
        self.fetchDataFromCoreData()
       
    }
    
   //mark: navigate to the Add contact Screen
    
    @objc func handleAddEvent() {
        let addVC = self.storyboard?.instantiateViewController(identifier: "AddContactViewController") as! AddContactViewController
        addVC.delegate = self
        addVC.contactsData = self.allContacts
        self.navigationController?.pushViewController(addVC, animated: true)
    }

    func fetchDataFromCoreData() {
        let fetchRequest: NSFetchRequest<Contact> = Contact.fetchRequest()
        do {
        let persons = try PersistenceServices.context.fetch(fetchRequest)
            for person in persons {
                if let startLetter = person.name?.first {
                   let letterString = "\(startLetter)"
                    self.allContacts[letterString]?.append(person)
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }catch {
            print("Failed to get Data")
        }
        
        
    }

    
}

extension ViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return contactSectionTitles.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              let personKey = self.contactSectionTitles[section]
               guard let personValues = self.allContacts[personKey] else { return 0}
               return personValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactTableViewCell
        
        let personKey = contactSectionTitles[indexPath.section]
        if let personValues = allContacts[personKey] {
        let contact = personValues[indexPath.row]
        cell.contact = contact
        }
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return contactSectionTitles[section]
    }
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {

        return self.contactSectionTitles

    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
}

extension ViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = self.storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        let personKey = contactSectionTitles[indexPath.section]
        if let personValues = allContacts[personKey] {
        let contact = personValues[indexPath.row]
        detailVC.contact = contact
        self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title: "Edit") { (action, view, completed) in
            let editVC = self.storyboard?.instantiateViewController(identifier: "AddContactViewController") as! AddContactViewController
            let personKey = self.contactSectionTitles[indexPath.section]
            if let personValues = self.allContacts[personKey] {
                let person = personValues[indexPath.row]
                editVC.person = person
                editVC.isEditable = true
                self.index = indexPath.section
                editVC.delegate = self
            }
            
           self.navigationController?.pushViewController(editVC, animated: true)
           completed(true)
        }
        let config = UISwipeActionsConfiguration(actions: [editAction])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let personKey = contactSectionTitles[indexPath.section]
       
           let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completed) in
                 for tuple in self.allContacts.enumerated() {
                     let key = tuple.element.key
                     let persons = tuple.element.value
                     for (index, _) in persons.enumerated() {
                        if indexPath.row == index && personKey == key {
                            
                        PersistenceServices.context.delete((self.allContacts[key]?.remove(at: index))!)
                           
                            do {
                                try PersistenceServices.context.save()
                            }catch {
                                print("Error in deleting Data")
                            }
                        }
                     }
                 }
                  completed(true)
                 DispatchQueue.main.async {
                     self.tableView.reloadData()
                 }
                
             }
             let config = UISwipeActionsConfiguration(actions: [deleteAction])
             config.performsFirstActionWithFullSwipe = false
             return config
    }
}

extension ViewController: DataSender {
    func update() {
        for tuple in self.allContacts.enumerated() {
            let key = tuple.element.key
            let persons = tuple.element.value
            
            for (index, person) in persons.enumerated() {
                guard let firstLetter =  person.name?.first else { return }
                
                let letter = "\(firstLetter)"
                
                if letter != key && index == index{
                    self.allContacts[key]?.remove(at: index)
                    self.allContacts[letter]?.append(person)
                }
               
            }
            
        }
        self.tableView.reloadData()
    }
    
    func sendContact(contact: Contact) {
        if let startLetter = contact.name?.first {
                   let letterString = "\(startLetter)"
                   self.allContacts[letterString]?.append(contact)
                   self.tableView.reloadData()
        }
    }
}
