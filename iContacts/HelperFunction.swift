//
//  HelperFunction.swift
//  iContacts
//
//  Created by Nuthan Raju Pesala on 12/05/20.
//  Copyright © 2020 Nuthan Raju Pesala. All rights reserved.
//

import Foundation
import UIKit

//Checking EmailID Format
func isValidEmail(emailid: String) -> Bool {
    let emailString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPred = NSPredicate(format: "SELF MATCHES %@", emailString)
    return emailPred.evaluate(with: emailid)
}

//Image From Initials

func imageFromInitials(firstName: String?, lastName: String?, withBlock: @escaping (_ image: UIImage)-> Void) {
    
    var string: String!
    var size = 36
    
    if firstName != nil && lastName != nil {
        string = String(firstName!.first!).uppercased() + String(lastName!.first!).uppercased()
        
    }else {
        string = String(firstName!.first!).uppercased()
        size = 72
    }
    
    let label = UILabel()
    label.frame.size = CGSize(width: 100, height: 100)
    label.text = string
    label.textAlignment = .center
    label.textColor = .white
    label.font = UIFont(name: label.font.familyName, size: CGFloat(size))
    label.backgroundColor = .blue
    label.layer.cornerRadius = 25
    
    UIGraphicsBeginImageContext(label.frame.size)
    label.layer.render(in: UIGraphicsGetCurrentContext()!)
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    withBlock(image!)
}
